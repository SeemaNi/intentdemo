//
//  NewsCustomCell.h
//  Wardrobe
//
//  Created by Rohit Kumar on 20/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsCustomCell : UITableViewCell
@property(nonatomic,strong)UIImageView *thumnailImage;
@property(nonatomic,strong)UILabel *headingLable;
@property(nonatomic,strong)UILabel *detailLable;
@property(nonatomic,strong)UIButton *righButton;
@property(nonatomic,strong)UIButton *arrowButton;
@end
