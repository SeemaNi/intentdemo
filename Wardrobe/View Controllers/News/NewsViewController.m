//
//  NewsViewController.m
//  Wardrobe
//
//  Created by Chetan Rajauria on 14/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "NewsViewController.h"
#import "HomeViewController.h"
#import "CoordiViewController.h"
#import "SNSViewController.h"
#import "SettingsViewController.h"
#import "NewsCustomCell.h"
@interface NewsViewController ()

@end

@implementation NewsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addButtonWithFrame:CGRectMake(20.0, 65.0, 42.0, 42.0) tag:1001 title:@"" image:[UIImage
        imageNamed:@"nice"]selectedimage:[UIImage imageNamed:@"nice_selected"]];
    [self addButtonWithFrame:CGRectMake(100.0, 65.0, 42.0, 42.0) tag:1002 title:@"" image:[UIImage imageNamed:@"comment"]selectedimage:[UIImage imageNamed:@"comment_selected"]];
    [self addButtonWithFrame:CGRectMake(180.0, 65.0, 42.0, 42.0) tag:1003 title:@"" image:[UIImage imageNamed:@"download"]selectedimage:[UIImage imageNamed:@"download_selected"]];
    [self addButtonWithFrame:CGRectMake(260.0, 65.0, 42.0, 42.0) tag:1004 title:@"" image:[UIImage imageNamed:@"wardrobe"]selectedimage:[UIImage imageNamed:@"wardrobe_selected"]];
    
    [self addLabelWithFrame:CGRectMake(9.0, 109.0, 61.0, 10.0) tag:2001 title:[NSString stringWithFormat:NSLocalizedString(@"Nice", nil)]];
    [self addLabelWithFrame:CGRectMake(90.0, 108.0, 61.0, 10.0) tag:2002 title:[NSString stringWithFormat:NSLocalizedString(@"Comment", nil)]];
    [self addLabelWithFrame:CGRectMake(170.0, 108.0, 61.0, 10.0) tag:2003 title:[NSString stringWithFormat:NSLocalizedString(@"Download", nil)]];
    [self addLabelWithFrame:CGRectMake(252.0, 108.0, 61.0, 10.0) tag:2004 title:[NSString stringWithFormat:NSLocalizedString(@"Wardrobe", nil)]];
    
    [self addSeparatorLineWithFrame:CGRectMake(0, self.topLogoBar.frame.origin.y+self.topLogoBar.frame.size.height+65, self.view.frame.size.width, 1.0)BGColor:[UIColor colorWithRed:190.0/255 green:155.0/255 blue:129.0/255 alpha:1.0]];
    [self addTableViewWithFrame:CGRectMake(0, self.topLogoBar.frame.origin.y+self.topLogoBar.frame.size.height+66, self.view.frame.size.width, 265) tag:4545];
    UIImage *sliderBGImage=[UIImage imageNamed:@"slider_bg"];
    UIImage *thumbImage=[UIImage imageNamed:@"slider_tracker"];
    [self addSliderWithFrame:CGRectMake(215, 185, sliderBGImage.size.width, sliderBGImage.size.height) BGImage:sliderBGImage trackerImg:thumbImage];
}

-(void)addSliderWithFrame:(CGRect)frame BGImage:(UIImage*)image trackerImg:(UIImage*)trackerImage
{
    UISlider *slider=[[UISlider alloc]initWithFrame:frame];
    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI_2);
    slider.transform = trans;
    [slider setThumbImage:trackerImage forState:UIControlStateNormal];
    [slider setMaximumTrackImage:image forState:UIControlStateNormal];
    [slider setMinimumTrackImage:image forState:UIControlStateNormal];
    [self.view addSubview:slider];
}
-(void)addTableViewWithFrame:(CGRect)frame tag:(int)tag
{
    UITableView *tableView=[[UITableView alloc]initWithFrame:frame];
    [tableView setTag:tag];
    [tableView setDataSource:(id)self];
    [tableView setDelegate:(id)self];
    [tableView setShowsVerticalScrollIndicator:NO];
    [tableView setBackgroundColor:[UIColor clearColor]];
    [tableView setSeparatorColor:[UIColor colorWithRed:190.0/255 green:155.0/255 blue:129.0/255 alpha:1.0]];
    [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    [self.view addSubview:tableView];
}

-(void)addSeparatorLineWithFrame:(CGRect)frame BGColor:(UIColor *)color
{
    UIView *lineView=[[UIView alloc]initWithFrame:frame];
    [lineView setBackgroundColor:color];
    [self.view addSubview:lineView];
}

-(void)addButtonWithFrame:(CGRect)frame tag:(int)tag title:(NSString*)title image:(UIImage *)image selectedimage:(UIImage *)image1
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(buttonAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    button.frame = frame;
    button.tag=tag;
    UIImage * buttonimg = image;
    [button setTitleColor:[UIColor colorWithRed:191.0/255 green:50.0/255 blue:29.0/255 alpha:1.0] forState:UIControlStateNormal];
    [button setBackgroundImage:buttonimg forState:UIControlStateNormal];
    
    UIImage * selectedimage =image1;
    [button setBackgroundImage:selectedimage forState:UIControlStateSelected];
    
    [self.view addSubview:button];
}

-(IBAction)buttonAction:(UIButton *)sender
{
    if (sender.tag ==001)
    {
        [self addBoxviewWithFrame:CGRectMake(04.0, 180.0, 311.0, 184.0) tag:100 title:@""];
    }
    if (sender.tag == 1002)
    {
        [self addCommentViewWithFrame:CGRectMake(14.0, 158.0, 292.0, 222.0) color:[UIColor colorWithRed:192.0/255 green:159.0/255 blue:133.0/255 alpha:0.8] tag:101];
    }
    if (sender.tag == 150)
    {
        NewsViewController * news = [[NewsViewController alloc]init];
        [self.navigationController pushViewController:news animated:NO];
    }
    if (sender.tag == 151)
    {
        NewsViewController * news = [[NewsViewController alloc]init];
        [self.navigationController pushViewController:news animated:NO];
    }
    if (sender.tag == 153)
    {
        NewsViewController * news = [[NewsViewController alloc]init];
        [self.navigationController pushViewController:news animated:NO];
    }
    UIButton * nice = (UIButton *)[self.view viewWithTag:1001];
    UIButton * comment = (UIButton *)[self.view viewWithTag:1002];
    UIButton * download = (UIButton *)[self.view viewWithTag:1003];
    UIButton * wardrobe = (UIButton *)[self.view viewWithTag:1004];
    UIView * commentview = (UIView *)[self.view viewWithTag:101];
    UIView * boxview = (UIView * )[self.view viewWithTag:100];
   // UIButton * cross = (UIButton *)[self.view viewWithTag:9999];
    
    if (sender.tag == 1001)
    {
        [nice setSelected:YES];
        [comment setSelected:NO];
        [download setSelected:NO];
        [wardrobe setSelected:NO];
    }
    if (sender.tag == 1002)
    {
        [nice setSelected:NO];
        [comment setSelected:YES];
        [download setSelected:NO];
        [wardrobe setSelected:NO];
    }
    if (sender.tag ==1003)
    {
        [nice setSelected:NO];
        [comment setSelected:NO];
        [download setSelected:YES];
        [wardrobe setSelected:NO];
    }
    if (sender.tag == 1004)
    {
        [nice setSelected:NO];
        [comment setSelected:NO];
        [download setSelected:NO];
        [wardrobe setSelected:YES];
    }
    UIView * smallview = (UIView *)[self.view viewWithTag:4000];
    if (sender.tag == 4000)
    {
        [smallview removeFromSuperview];
    }
    if (sender.tag == 9999)
    {
        [commentview removeFromSuperview];
        [smallview removeFromSuperview];
        [boxview removeFromSuperview];
        
    }
    UIView * view = (UIView *)[self.view viewWithTag:2121];
    if (sender.tag == 1005)
    {
        [view removeFromSuperview];
    }
    if (sender.tag == 1003)
    {
        UIView * view = (UIView *)[self.view viewWithTag:2121];
        if (view)
        {
            return;
        }
        view = [[UIView alloc]init];
        view.frame = CGRectMake(05.0, 176.0, 310.0, 180.0);
        [view setBackgroundColor:[UIColor colorWithRed:164.0/255 green:138.0/255 blue:111.0/255 alpha:0.9]];
        [view.layer setCornerRadius:5.0];
        [view.layer setBorderColor:[UIColor colorWithRed:171.0/255 green:133.0/255 blue:88.0/255 alpha:1.0].CGColor];
        view.tag =2121;
        [view.layer setBorderWidth:1.0];
        [view setOpaque:YES];
        [self.view addSubview:view];
        
        UILabel * label = [[UILabel alloc]init];
        label.frame = CGRectMake(33.0, 64.0, 262.0, 30.0);
        [label setTextColor:[UIColor colorWithRed:254.0/255 green:253.0/255 blue:249.0/255 alpha:1.0]];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setText:@"Wardrobe......is good!!!"];
        [label setFont:[UIFont fontWithName:@"Times New Roman" size:16]];
        [view addSubview:label];
        
        UILabel * label1 = [[UILabel alloc]init];
        label1.frame = CGRectMake(50.0, 84.0, 200.0, 30.0);
        [label1 setText:@"Are you Sure"];
        [label1 setTextAlignment:NSTextAlignmentCenter];
        [label1 setTextColor:[UIColor colorWithRed:254.0/255 green:253.0/255 blue:249.0/255 alpha:1.0]];
        [label1 setFont:[UIFont fontWithName:@"Times New Roman" size:16]];
        [view addSubview:label1];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self
                   action:@selector(buttonAction:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:@"Ok" forState:UIControlStateNormal];
        button.frame = CGRectMake(124.0, 147.0, 60.0, 20.0);
        button.tag=1005;
        UIImage * buttonimg = [UIImage imageNamed:@"Ok_btn"];
        
        [button setTitleColor:[UIColor colorWithRed:157.0/255 green:118.0/255 blue:63.0/255 alpha:1.0] forState:UIControlStateNormal];
        [button setBackgroundImage:buttonimg forState:UIControlStateNormal];
        [view addSubview:button];
    }
}

-(void)addBoxviewWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title
{
    UIView *boxview = [self.view viewWithTag:tag];
    if (boxview)
    {
        return;
    }
    boxview = [[UIView alloc]initWithFrame:frame];
    boxview.backgroundColor = [UIColor colorWithRed:155.0/255 green:120.0/255 blue:91.0/255 alpha:0.8];
    boxview.opaque = YES;
    boxview.tag = tag;
    [boxview.layer setCornerRadius:5.0];
    [self.view addSubview:boxview];
    
    UITextView * textview = [[UITextView alloc]initWithFrame:CGRectMake(15.0, 30.0, 277.0, 60.0)];
    textview.backgroundColor = [UIColor whiteColor];
    [textview.layer setCornerRadius:5.0];
    [textview setDelegate:self];
    [boxview addSubview:textview];
    
    
    UILabel * lblmsg = [[UILabel alloc]initWithFrame:CGRectMake(05.0, 05.0, 90.0, 20.0)];
    [lblmsg setBackgroundColor:[UIColor clearColor]];
    [lblmsg setText:[NSString stringWithFormat:NSLocalizedString(@"Comment", nil)]];
    lblmsg.textColor = [UIColor colorWithRed:252.0/255 green:238.0/255 blue:222.0/255 alpha:1.0];
    lblmsg.font = [UIFont fontWithName:@"Times New Roman" size:14];
    [boxview addSubview:lblmsg];
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(buttonAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:[NSString stringWithFormat:NSLocalizedString(@"Submit",nil)]forState:UIControlStateNormal];
    button.frame = CGRectMake(111.0, 145.0, 90.0, 20.0);
    button.tag=150;
    [button setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bigbtn"]]];
    
    [button setTitleColor:[UIColor colorWithRed:164.0/255 green:124.0/255 blue:65.0/255 alpha:1.0] forState:UIControlStateNormal];
    [boxview addSubview:button];
    
    UIButton * cross_button = [UIButton buttonWithType:UIButtonTypeCustom];
    [cross_button addTarget:self
               action:@selector(buttonAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [cross_button setTitle:[NSString stringWithFormat:NSLocalizedString(@"",nil)]forState:UIControlStateNormal];
    cross_button.frame = CGRectMake(280.0, 05.0, 13.0, 14.0);
    cross_button.tag=9999;
    [cross_button setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"btn_cross"]]];
    
    [cross_button setTitleColor:[UIColor colorWithRed:191.0/255 green:50.0/255 blue:29.0/255 alpha:1.0] forState:UIControlStateNormal];
    [boxview addSubview:cross_button];
}

// IMPORTANt CODE........................................................................

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    UIView * boxview = (UIView *)[self.view viewWithTag:100];
//    UIView * smallboxview = (UIView * )[self.view viewWithTag:4000];
//    UIView * commentview = (UIView*)[self.view viewWithTag:101];
//
//    UITouch *touch=[[event allTouches]anyObject];
//    
//    if (![touch.view isEqual: boxview])
//    {
//        [boxview removeFromSuperview];
//    }
//    
//    if (![touch.view isEqual:commentview])
//    {
//    [commentview removeFromSuperview];
//    }
//    
//    if (![touch.view isEqual:smallboxview])
//    {
//        [smallboxview removeFromSuperview];
//    }
//}
//.......................................................................................


-(void)addLabelWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title
{
    UILabel* label = [[UILabel alloc] initWithFrame: frame];
    [label setText: title];
    label.tag =tag;
    //label.textColor = [UIColor colorWithRed:252.0/255 green:238.0/255 blue:222.0/255 alpha:1.0];
    [label setTextAlignment:NSTextAlignmentCenter];
    label.font = [UIFont fontWithName:@"Times New Roman" size:10];
    [label setTextColor: [UIColor colorWithRed:190.0/255 green:155.0/255 blue:129.0/255 alpha:1.0]];
    [self.view addSubview: label];
}

-(void)addCommentViewWithFrame:(CGRect)frame color:(UIColor *)color tag:(int)tag
{

    UIView * commentview = [self.view viewWithTag:101];
    if (commentview)
    {
        return;
    }
        commentview = [[UIView alloc]init];
        commentview.frame = frame;
        commentview.backgroundColor = color;
        commentview.opaque = YES;
        [commentview.layer setCornerRadius:5.0];
        commentview.tag = 101;
        [self.view addSubview:commentview];
    
    
    UIView * smallcommentview = [self.view viewWithTag:102];
    if (smallcommentview)
    {
        return;
    }
        smallcommentview = [[UIView alloc]init];
        smallcommentview.frame = CGRectMake(08.0, 10.0, 278.0, 70.0);
        smallcommentview.backgroundColor = [UIColor colorWithRed:165.0/255 green:126.0/255 blue:97.0/255 alpha:0.8];
        smallcommentview.layer.borderColor = [UIColor colorWithRed:163.0/255 green:114.0/255 blue:59.0/255 alpha:1.0].CGColor;
        smallcommentview.layer.borderWidth = 1.0f;
        [smallcommentview.layer setCornerRadius:5.0];
        [commentview addSubview:smallcommentview];
    
    
    UITextView * textview = [[UITextView alloc]init];
    textview.frame = CGRectMake(08.0, 90.0, 278.0, 91.0);
    textview.backgroundColor = [UIColor whiteColor];
    [textview.layer setCornerRadius:5.0];
    textview.tag = 201;
    [textview setDelegate:self];
    [commentview addSubview:textview];
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btn addTarget:self
             action:@selector(buttonAction:)
   forControlEvents:UIControlEventTouchUpInside];
    [btn setTitle:@"Submit" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont systemFontOfSize:14]];
    btn.frame = CGRectMake(100.0, 190.0, 92.0, 20.0);
    [btn setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bigbtn"]]];
    [btn setTitleColor:[UIColor colorWithRed:157.0/255 green:118.0/255 blue:60.0/255 alpha:1.0] forState:UIControlStateNormal];
    btn.tag = 153;
    [commentview addSubview:btn];
    
    UIButton * cross = [UIButton buttonWithType:UIButtonTypeCustom];
    [cross addTarget:self
            action:@selector(buttonAction:)forControlEvents:UIControlEventTouchUpInside];
    [cross setTitle:@"" forState:UIControlStateNormal];
    cross.frame = CGRectMake(265.0, 05.0, 13.0, 14.0);
    [cross setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"btn_cross"]]];
    [cross setTitleColor:[UIColor colorWithRed:191.0/255 green:50.0/255 blue:29.0/255 alpha:1.0] forState:UIControlStateNormal];
    cross.tag = 9999;
    [commentview addSubview:cross];
    
    UILabel * lbl = [[UILabel alloc]initWithFrame:CGRectMake(26.0, 19.0, 230.0, 15.0)];
    [lbl setText:[NSString stringWithFormat:NSLocalizedString(@"Fh&%$#@*&%%$#@#$#$#$#$", nil)]];
    lbl.textColor = [UIColor colorWithRed:252.0/255 green:238.0/255 blue:223.0/255 alpha:1.0];
    lbl.font = [UIFont fontWithName:@"Times New Roman" size:12];
    [commentview addSubview:lbl];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView.tag == 201)
    {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-110, self.view.frame.size.width, self.view.frame.size.height)];
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView.tag == 201)
    {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+110, self.view.frame.size.width, self.view.frame.size.height)];
    }
}

-(void)addSmallViewBox
{
    UIView * smallbox = [self.view viewWithTag:4000];
    if (smallbox)
    {
        return;
    }
    smallbox = [[UIView alloc]initWithFrame:CGRectMake(05.0, 177.0, 310.0, 114.0)];
    smallbox.backgroundColor =[UIColor colorWithRed:155.0/255 green:120.0/255 blue:91.0/255 alpha:0.8];
    [smallbox.layer setOpaque:YES];
    [smallbox.layer setCornerRadius:0.5];
    smallbox.tag = 4000;
    
    UIButton * btn1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btn1 addTarget:self
             action:@selector(buttonAction:)
   forControlEvents:UIControlEventTouchUpInside];
    [btn1 setTitle:@"Done" forState:UIControlStateNormal];
    btn1.frame = CGRectMake(86.0, 60.0, 60.0, 20.0);
    btn1.tag=150;
    [btn1 setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"smallbtn1"]]];
    [btn1 setTitleColor:[UIColor colorWithRed:164.0/255 green:124.0/255 blue:65.0/255 alpha:1.0] forState:UIControlStateNormal];
    [smallbox addSubview:btn1];
    
    
    UILabel * lbl = [[UILabel alloc]initWithFrame:CGRectMake(26.0, 19.0, 230.0, 15.0)];
    [lbl setText:[NSString stringWithFormat:NSLocalizedString(@"Fh&%$#@*&%%$#@#$#$#$#$", nil)]];
    lbl.textColor = [UIColor colorWithRed:252.0/255 green:238.0/255 blue:223.0/255 alpha:1.0];
    lbl.font = [UIFont fontWithName:@"Times New Roman" size:12];
    [smallbox addSubview:lbl];
    
    UIButton * btn2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btn2 addTarget:self
             action:@selector(buttonAction:)
   forControlEvents:UIControlEventTouchUpInside];
    [btn2 setTitle:@"Exit" forState:UIControlStateNormal];
    btn2.frame = CGRectMake(167.0, 60.0, 60.0, 20.0);
    btn2.tag=151;
    [btn2 setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"smallbtn2"]]];
    [btn2 setTitleColor:[UIColor colorWithRed:164.0/255 green:124.0/255 blue:65.0/255 alpha:1.0] forState:UIControlStateNormal];
    [smallbox addSubview:btn2];
    
    UIButton * cross = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [cross addTarget:self
            action:@selector(buttonAction:)
  forControlEvents:UIControlEventTouchUpInside];
    UIImage *image=[UIImage imageNamed:@"btn_cross"];
    [cross setTitle:@"" forState:UIControlStateNormal];
    cross.frame = CGRectMake(270.0, 003.0, image.size.width, image.size.height);
    [cross setBackgroundColor:[UIColor colorWithPatternImage:image]];
    [cross setTitleColor:[UIColor colorWithRed:191.0/255 green:50.0/255 blue:29.0/255 alpha:1.0] forState:UIControlStateNormal];
    cross.tag = 9999;
    [smallbox addSubview:cross];
    [self.view addSubview:smallbox];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}


#pragma mark UITableViewDelegate and UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 8;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"CellIdentifier";
    NewsCustomCell *cell =(NewsCustomCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[[NewsCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    [cell.righButton addTarget:self
               action:@selector(buttonAction:)
     forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self addSmallViewBox];
    NSLog(@"didSelectRowAtIndexPath clicked");
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
