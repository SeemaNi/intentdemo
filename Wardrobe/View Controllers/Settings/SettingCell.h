//
//  SettingCell.h
//  Wardrobe
//
//  Created by Rohit Kumar on 19/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingCell : UITableViewCell
@property(nonatomic,strong)UILabel *titleLable;
@property(nonatomic,strong)UIButton *accessoryButton;

@end
