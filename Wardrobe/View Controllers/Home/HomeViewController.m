//
//  HomeViewController.m
//  Wardrobe
//
//  Created by Chetan Rajauria on 14/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "HomeViewController.h"
#import "CoordiViewController.h"
#import "SNSViewController.h"
#import "NewsViewController.h"
#import "SettingsViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self.view setBackgroundColor:[UIColor whiteColor]];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"home_back"]]];
    [self addButtonWithFrame:CGRectMake(08.0, 395.0, 50.0, 50.0) tag:1001 title:@"" image:[UIImage imageNamed:@"camera_icon"]];
    [self addButtonWithFrame:CGRectMake(74.0, 395.0, 50.0, 50.0) tag:1002 title:@"" image:[UIImage imageNamed:@"coordi_icon"]];
    [self addButtonWithFrame:CGRectMake(137.0, 395.0, 50.0, 50.0) tag:1003 title:@"" image:[UIImage imageNamed:@"sns_icon"]];
    [self addButtonWithFrame:CGRectMake(201.0, 395.0, 50.0, 50.0) tag:1004 title:@"" image:[UIImage imageNamed:@"news_icon"]];
    [self addButtonWithFrame:CGRectMake(262.0, 395.0, 50.0, 50.0) tag:1005 title:@"" image:[UIImage imageNamed:@"setting_icon"]];
    
    // Do any additional setup after loading the view.
}

-(void)addButtonWithFrame:(CGRect)frame tag:(int)tag title:(NSString*)title image:(UIImage *)image
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:@selector(buttonAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    button.frame = frame;
    button.tag=tag;
    UIImage * buttonimg = image;
    [button setBackgroundImage:buttonimg forState:UIControlStateNormal];
    //[UIButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [self.view addSubview:button];
    //button.layer.borderColor=[[UIColor blackColor]CGColor];
    //button.layer.borderWidth= 1.0f;
}

-(IBAction)buttonAction:(UIButton*)sender
{
    if (sender.tag ==1001)
    {
        
    }
    if (sender.tag == 1002)
    {
        CoordiViewController * coordi = [[CoordiViewController alloc]init];
        [self.navigationController pushViewController:coordi animated:NO];
    }
    if (sender.tag == 1003)
    {
        SNSViewController * snsview = [[SNSViewController alloc]init];
        [self.navigationController pushViewController:snsview animated:NO];
    }
    if (sender.tag == 1004)
    {
        NewsViewController * news = [[NewsViewController alloc]init];
        [self.navigationController pushViewController:news animated:NO];
    }
    if (sender.tag == 1005)
    {
        SettingsViewController * setting = [[SettingsViewController alloc]init];
        [self.navigationController pushViewController:setting animated:NO];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
