//
//  Coordi_Coordi.m
//  Wardrobe
//
//  Created by Chetan Rajauria on 17/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "Coordi_Coordi.h"
#import "CoordiViewController.h"

@interface Coordi_Coordi ()
{
    
    NSMutableArray * image_array;
    UIScrollView * scroller;

}
@end

@implementation Coordi_Coordi

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    image_array =[[NSMutableArray alloc]initWithObjects:@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",@"coordi_category1.jpg",nil];
    
    
    
    scroller = [[UIScrollView alloc]initWithFrame:CGRectMake(29.0, 62.0, 295.0, 330.0)];
    scroller.delegate = (id)self;
    scroller.showsVerticalScrollIndicator = YES;
    scroller.scrollEnabled = YES;
    scroller.userInteractionEnabled =YES;
    int contentHeight=(image_array.count%3!=0)?(image_array.count/3)+1:image_array.count/3;
    [scroller setContentSize:CGSizeMake(scroller.frame.size.width, contentHeight*100+30)];
    scroller.backgroundColor = [UIColor clearColor];
    [self.view addSubview:scroller];
    
    
    [self addButtonImages];
    UIImage *sliderBGImage=[UIImage imageNamed:@"slider_bg"];
    UIImage *thumbImage=[UIImage imageNamed:@"slider_tracker"];
    [self addSliderWithFrame:CGRectMake(218.0, 170.0, sliderBGImage.size.width, sliderBGImage.size.height) BGImage:sliderBGImage trackerImg:thumbImage];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addButtonImages
{
    float horizontal = 20.0;
    float vertical = 03.0;
    UILabel * lbl_images;
    
    for(int i=0; i<[image_array count]; i++)
    {
        if((i%3) == 0 && i!=0)
        {
            //horizontal = horizontal + 0.0+10.0;
            horizontal = 20.0;
            vertical = vertical + 80.0 + 22.0;
        }
        UIButton * buttonImage = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonImage setFrame:CGRectMake(horizontal, vertical, 67.0, 70.0)];
        [buttonImage setTag:i];
        
        UIView * view_border = [[UIView alloc]initWithFrame:CGRectMake(horizontal-2, vertical+9, 70.0, 69.0)];
        
        [view_border.layer setShadowColor:[UIColor blackColor].CGColor];
        [[view_border layer]setBackgroundColor:[UIColor whiteColor].CGColor];
        [view_border.layer setShadowOpacity:0.8];
        [view_border.layer setShadowRadius:3.0];
        [view_border.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
        
        [scroller addSubview:view_border];
        
        lbl_images = [[UILabel alloc]init];
        [lbl_images setFrame:CGRectMake(horizontal, vertical+60, 67.0, 17.0)];
        [lbl_images setTag:i];
        [lbl_images setTextAlignment:NSTextAlignmentCenter];
        [lbl_images setText:@"Dresss"];
        [lbl_images setTextColor:[UIColor colorWithRed:160.0/255 green:156.0/255 blue:155.0/255 alpha:1.0]];
        lbl_images.font = [UIFont fontWithName:@"Times New Roman" size:10];
        [lbl_images setBackgroundColor:[UIColor colorWithRed:248.0/255 green:242.0/255 blue:242.0/255 alpha:1.0]];
        
        [scroller addSubview:lbl_images];
        
        [buttonImage setImage:[UIImage imageNamed:[image_array objectAtIndex:i]] forState:UIControlStateNormal];
        
        [buttonImage addTarget:self action:@selector(button:) forControlEvents:UIControlEventTouchUpInside];
        
        [scroller addSubview:buttonImage];
        horizontal = horizontal + 80.0 + 12.0;
        
        
    }
}

-(void)addSliderWithFrame:(CGRect)frame BGImage:(UIImage*)image trackerImg:(UIImage*)trackerImage
{
    UISlider *slider=[[UISlider alloc]initWithFrame:frame];
    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI_2);
    slider.transform = trans;
    [slider setThumbImage:trackerImage forState:UIControlStateNormal];
    [slider setMaximumTrackImage:image forState:UIControlStateNormal];
    [slider setMinimumTrackImage:image forState:UIControlStateNormal];
    [self.view addSubview:slider];
}


-(void)addButtonWithFrame:(CGRect)frame tag:(int)tag title:(NSString*)title image:(UIImage *)image
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:@selector(button:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    button.frame = frame;
    button.tag=tag;
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    UIImage * buttonimg = image;
    [button setBackgroundImage:buttonimg forState:UIControlStateNormal];
    //[UIButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [self.view addSubview:button];
    //button.layer.borderColor=[[UIColor blackColor]CGColor];
    //button.layer.borderWidth= 1.0f;
}

-(IBAction)button:(UIButton*)sender
{
    if (sender.tag == 1012)
    {
        CoordiViewController *coordi_home = (CoordiViewController *)[self.navigationController.viewControllers objectAtIndex:2];
        [self.navigationController popToViewController:coordi_home animated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
