//
//  Coordi_BrandShop.m
//  Wardrobe
//
//  Created by Chetan Rajauria on 17/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "Coordi_BrandShop.h"
#import "CoordiViewController.h"

@interface Coordi_BrandShop ()
{
    UIScrollView * scroller;
    UIScrollView * scroller1;
}

@end

@implementation Coordi_BrandShop

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addButtonWithFrame:CGRectMake(0.0, 60.0, 53.0, 32.0) tag:1001 title:@"" image:[UIImage imageNamed:@"brand_icon1"]image:[UIImage imageNamed:@"light_all"]color:[UIColor clearColor]];
    [self addButtonWithFrame:CGRectMake(53.0, 60.0, 53.0, 32.0) tag:1002 title:@"" image:[UIImage imageNamed:@"brand_icon2"]image:[UIImage imageNamed:@"light_men"]color:[UIColor clearColor]];
    [self addButtonWithFrame:CGRectMake(106.0, 60.0, 54.0, 32.0) tag:1003 title:@"" image:[UIImage imageNamed:@"brand_icon3"]image:[UIImage imageNamed:@"light_ladies"]color:[UIColor clearColor]];
    [self addButtonWithFrame:CGRectMake(160.0, 60.0, 54.0, 32.0) tag:1004 title:@"" image:[UIImage imageNamed:@"brand_icon4"]image:[UIImage imageNamed:@"light_group"]color:[UIColor clearColor]];
    [self addButtonWithFrame:CGRectMake(213.0, 60.0, 54.0, 32.0) tag:1005 title:@"" image:[UIImage imageNamed:@"dark_brand"]image:[UIImage imageNamed:@"light_brand"]color:[UIColor clearColor]];
    [self addButtonWithFrame:CGRectMake(267.0, 60.0, 53.0, 32.0) tag:1006 title:@"" image:[UIImage imageNamed:@"brand_icon6"]image:[UIImage imageNamed:@"light_shops"]color:[UIColor clearColor]];
    
    
    [self addLabelWithFrame:CGRectMake(0.0, 70.0, 53.0, 32.0) tag:2001 title:@"All" font:[UIFont fontWithName:@"Times New Roman" size:10.0]color:[UIColor colorWithRed:249.0/255 green:215.0/255 blue:174.0/255 alpha:1.0]];
    [self addLabelWithFrame:CGRectMake(53.0, 70.0, 53.0, 32.0) tag:2002 title:@"Men" font:[UIFont fontWithName:@"Times New Roman" size:10.0]color:[UIColor colorWithRed:249.0/255 green:215.0/255 blue:174.0/255 alpha:1.0]];
    [self addLabelWithFrame:CGRectMake(106.0, 70.0, 53.0, 32.0) tag:2003 title:@"Ladies" font:[UIFont fontWithName:@"Times New Roman" size:10.0]color:[UIColor colorWithRed:249.0/255 green:215.0/255 blue:174.0/255 alpha:1.0]];
    [self addLabelWithFrame:CGRectMake(160.0, 70.0, 53.0, 32.0) tag:2004 title:@"Kids" font:[UIFont fontWithName:@"Times New Roman" size:10.0]color:[UIColor colorWithRed:249.0/255 green:215.0/255 blue:174.0/255 alpha:1.0]];
    [self addLabelWithFrame:CGRectMake(213.0, 70.0, 53.0, 32.0) tag:2005 title:@"Brand" font:[UIFont fontWithName:@"Times New Roman" size:10.0]color:[UIColor colorWithRed:249.0/255 green:215.0/255 blue:174.0/255 alpha:1.0]];
    [self addLabelWithFrame:CGRectMake(267.0, 70.0, 53.0, 32.0) tag:2006 title:@"Shops" font:[UIFont fontWithName:@"Times New Roman" size:10.0]color:[UIColor colorWithRed:249.0/255 green:215.0/255 blue:174.0/255 alpha:1.0]];
    
    [self addTextFieldWithFrame:CGRectMake(40.0, 100.0, 240.0, 20.0) tag:301 title:@""];
    
     [self addSeparatorLineWithFrame:CGRectMake(0.0, 125.0, 320.0, 1.0)BGColor:[UIColor colorWithRed:190.0/255 green:155.0/255 blue:129.0/255 alpha:1.0]];
    [self addScrollViewWithFrame:CGRectMake(5.0, 125.0, 315.0, 265.0)color:[UIColor clearColor]];
    
    
    UIImage *sliderBGImage=[UIImage imageNamed:@"slider_bg"];
    UIImage *thumbImage=[UIImage imageNamed:@"slider_tracker"];
    [self addSliderWithFrame:CGRectMake(215.0, 200.0, sliderBGImage.size.width, sliderBGImage.size.height) BGImage:sliderBGImage trackerImg:thumbImage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)addScrollViewWithFrame:(CGRect)frame color:(UIColor *)color
{
    scroller = [[UIScrollView alloc]init];
    scroller.frame = frame;        //  :CGRectMake(5.0, 125.0, 315.0, 265.0)];
    scroller.delegate = (id)self;
    scroller.showsVerticalScrollIndicator = YES;
    scroller.scrollEnabled = YES;
    scroller.userInteractionEnabled =YES;
    scroller.contentSize =CGSizeMake(0.0, 1000.0);
    scroller.backgroundColor = color;
    [self.view addSubview:scroller];
}

-(void)addScrollView1WithFrame:(CGRect)frame color:(UIColor *)color
{
    scroller1 = [[UIScrollView alloc]init];
    scroller1.frame = frame;        //  :CGRectMake(5.0, 125.0, 315.0, 265.0)];
    scroller1.delegate = (id)self;
    scroller1.showsVerticalScrollIndicator = YES;
    scroller1.scrollEnabled = YES;
    scroller1.userInteractionEnabled =YES;
    scroller1.contentSize =CGSizeMake(0.0, 320.0);
    scroller1.backgroundColor = color;
    [self.view addSubview:scroller1];
}


-(void)addSliderWithFrame:(CGRect)frame BGImage:(UIImage*)image trackerImg:(UIImage*)trackerImage
{
    UISlider *slider=[[UISlider alloc]initWithFrame:frame];
    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI_2);
    slider.transform = trans;
    [slider setThumbImage:trackerImage forState:UIControlStateNormal];
    [slider setMaximumTrackImage:image forState:UIControlStateNormal];
    [slider setMinimumTrackImage:image forState:UIControlStateNormal];
    [self.view addSubview:slider];
}


-(void)addTextFieldWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title
{
    UITextField *tf = [[UITextField alloc] initWithFrame:frame];
    tf.textColor = [UIColor blackColor];
    tf.font = [UIFont fontWithName:@"Times New Roman" size:12];
    tf.backgroundColor=[UIColor whiteColor];
    [tf.layer setCornerRadius:13.50];
    tf.placeholder = @"Search";
    
    
    tf.tag=tag;
//    UIImageView *searchIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"searchicon"]];
//    searchIcon.frame = CGRectMake(45, 100, 12, 12);
    
//   UITextField * myTextField = [[UITextField alloc] initWithFrame:CGRectMake(45.0, 100.0, 12.0, 12.0)];
//    
    
    [tf setLeftViewMode:UITextFieldViewModeAlways];
    tf.leftView= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"searchicon"]];
    
    tf.layer.borderColor=[[UIColor colorWithRed:228.0/255 green:219.0/255 blue:212.0/255 alpha:1.0]CGColor];
    tf.layer.borderWidth= 1.0f;
    [tf setDelegate:(id)self];
    [self.view addSubview:tf];
}

-(void)addSeparatorLineWithFrame:(CGRect)frame BGColor:(UIColor *)color
{
    UIView *lineView=[[UIView alloc]initWithFrame:frame];
    [lineView setBackgroundColor:color];
    [self.view addSubview:lineView];
    [scroller1 addSubview:lineView];
}

-(void)addLabelWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title font:(UIFont *)font color:(UIColor *)color
{
    UILabel* label = [[UILabel alloc] initWithFrame: frame];
    [label setText: title];
    label.tag =tag;
    [label setTextAlignment:NSTextAlignmentCenter];
    label.textColor = color;
    label.font = font;
    [self.view addSubview: label];
    [scroller1 addSubview:label];
}

-(void)addButtonWithFrame:(CGRect)frame tag:(int)tag title:(NSString*)title image:(UIImage *)unselected image:(UIImage *)selected color:(UIColor *)color
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(button:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    button.frame = frame;
    button.tag=tag;
    [button.titleLabel setFont:[UIFont fontWithName:@"Times New Roman" size:10]];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    UIImage * buttonimg = unselected;
    [button setBackgroundImage:buttonimg forState:UIControlStateNormal];
    
    UIImage * buttonimg1 = selected;
    [button setBackgroundImage:buttonimg1 forState:UIControlStateSelected];
    //[UIButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [self.view addSubview:button];
    [scroller1 addSubview:button];
}

-(void)addImageViewWithFrame:(CGRect)frame image:(UIImage *)image
{
    UIImageView * imageview = [[UIImageView alloc]init];
    imageview.frame = frame;
    imageview.image = image;
    [scroller1 addSubview:imageview];
}

-(IBAction)button:(UIButton*)sender
{
    if (sender.tag == 1012)
    {
        CoordiViewController * coordi_home = (CoordiViewController *)[self.navigationController.viewControllers objectAtIndex:2];
        [self.navigationController popToViewController:coordi_home animated:YES];
    }
    UIButton * all = (UIButton *)[self.view viewWithTag:1001];
    UIButton * men = (UIButton *)[self.view viewWithTag:1002];
    UIButton * ladies = (UIButton *)[self.view viewWithTag:1003];
    UIButton * group = (UIButton *)[self.view viewWithTag:1004];
    UIButton * brand = (UIButton *)[self.view viewWithTag:1005];
    UIButton * shop = (UIButton *)[self.view viewWithTag:1006];
   
    UILabel * lbl_all = (UILabel *)[self.view viewWithTag:2001];
    [lbl_all setTextColor:[UIColor colorWithRed:249.0/255 green:215.0/255 blue:174.0/255 alpha:1.0]];
    
    UILabel * lbl_men = (UILabel *)[self.view viewWithTag:2002];
    [lbl_men setTextColor:[UIColor colorWithRed:249.0/255 green:215.0/255 blue:174.0/255 alpha:1.0]];
    
    UILabel * lbl_ladies = (UILabel *)[self.view viewWithTag:2003];
    [lbl_ladies setTextColor:[UIColor colorWithRed:249.0/255 green:215.0/255 blue:174.0/255 alpha:1.0]];
    
    UILabel * lbl_group = (UILabel *)[self.view viewWithTag:2004];
    [lbl_group setTextColor:[UIColor colorWithRed:249.0/255 green:215.0/255 blue:174.0/255 alpha:1.0]];
    
    UILabel * lbl_brand = (UILabel *)[self.view viewWithTag:2005];
    [lbl_brand setTextColor:[UIColor colorWithRed:249.0/255 green:215.0/255 blue:174.0/255 alpha:1.0]];
    
    UILabel * lbl_shop =(UILabel *)[self.view viewWithTag:2006];
    [lbl_shop setTextColor:[UIColor colorWithRed:249.0/255 green:215.0/255 blue:174.0/255 alpha:1.0]];

    if (sender.tag == 1001)
    {
        [all setSelected:YES];
        [men setSelected:NO];
        [ladies setSelected:NO];
        [group setSelected:NO];
        [brand setSelected:NO];
        [shop setSelected:NO];
        [lbl_all setTextColor:[UIColor colorWithRed:153.0/255 green:103.0/255 blue:40.0/255 alpha:1.0]];
        
    }
    if (sender.tag == 1002)
    {
        [all setSelected:NO];
        [men setSelected:YES];
        [ladies setSelected:NO];
        [group setSelected:NO];
        [brand setSelected:NO];
        [shop setSelected:NO];
        [lbl_men setTextColor:[UIColor colorWithRed:153.0/255 green:103.0/255 blue:40.0/255 alpha:1.0]];
    }
    if (sender.tag == 1003)
    {
        [all setSelected:NO];
        [men setSelected:NO];
        [ladies setSelected:YES];
        [group setSelected:NO];
        [brand setSelected:NO];
        [shop setSelected:NO];
        [lbl_ladies setTextColor:[UIColor colorWithRed:153.0/255 green:103.0/255 blue:40.0/255 alpha:1.0]];
    }
    if (sender.tag == 1004)
    {
        [all setSelected:NO];
        [men setSelected:NO];
        [ladies setSelected:NO];
        [group setSelected:YES];
        [brand setSelected:NO];
        [shop setSelected:NO];
        [lbl_group setTextColor:[UIColor colorWithRed:153.0/255 green:103.0/255 blue:40.0/255 alpha:1.0]];
    }
    if (sender.tag == 1005)
    {
        [all setSelected:NO];
        [men setSelected:NO];
        [ladies setSelected:NO];
        [group setSelected:NO];
        [brand setSelected:YES];
        [shop setSelected:NO];
        [lbl_brand setTextColor:[UIColor colorWithRed:153.0/255 green:103.0/255 blue:40.0/255 alpha:1.0]];
    }
    if (sender.tag == 1006)
    {
        [all setSelected:NO];
        [men setSelected:NO];
        [ladies setSelected:NO];
        [group setSelected:NO];
        [brand setSelected:NO];
        [shop setSelected:YES];
        [lbl_shop setTextColor:[UIColor colorWithRed:153.0/255 green:103.0/255 blue:40.0/255 alpha:1.0]];
        
        [self addScrollView1WithFrame:CGRectMake(0.0, 95.0, 320.0, 295.0) color:[UIColor colorWithRed:252.0/255 green:238.0/255 blue:222.0/255 alpha:1.0]];
        [self addSeparatorLineWithFrame:CGRectMake(0.0, 30.0, 320.0, 1.0)BGColor:[UIColor colorWithRed:206.0/255 green:209.0/255 blue:191.0/255 alpha:1.0]];
        [self addImageViewWithFrame:CGRectMake(94.0, 36.0, 133.0, 137.0)image:[UIImage imageNamed:@"redshirt"]];
        
        [self addLabelWithFrame:CGRectMake(40.0, 176.0, 80.0, 15.0) tag:2001 title:@"Product Name :" font:[UIFont fontWithName:@"Times New Roman" size:12] color:[UIColor colorWithRed:112.0/255 green:102.0/255 blue:93.0/255 alpha:1.0]];
        [self addLabelWithFrame:CGRectMake(19.0, 200.0, 80.0, 15.0) tag:2002 title:@"Color :" font:[UIFont fontWithName:@"Times New Roman" size:12] color:[UIColor colorWithRed:112.0/255 green:102.0/255 blue:93.0/255 alpha:1.0]];
        [self addLabelWithFrame:CGRectMake(15.0, 224.0, 80.0, 15.0) tag:2003 title:@"Size :" font:[UIFont fontWithName:@"Times New Roman" size:12] color:[UIColor colorWithRed:112.0/255 green:102.0/255 blue:93.0/255 alpha:1.0]];
        [self addLabelWithFrame:CGRectMake(17.0, 249.0, 80.0, 15.0) tag:2004 title:@"Price :" font:[UIFont fontWithName:@"Times New Roman" size:12] color:[UIColor colorWithRed:112.0/255 green:102.0/255 blue:93.0/255 alpha:1.0]];
        [self addLabelWithFrame:CGRectMake(39.0, 191.0, 230.0, 2.0) tag:2005 title:@"---------------------------------------------------------" font:[UIFont fontWithName:@"Times New Roman" size:12] color:[UIColor colorWithRed:112.0/255 green:102.0/255 blue:93.0/255 alpha:1.0]];
        [self addLabelWithFrame:CGRectMake(39.0, 215.0, 230.0, 2.0) tag:2005 title:@"---------------------------------------------------------" font:[UIFont fontWithName:@"Times New Roman" size:12] color:[UIColor colorWithRed:112.0/255 green:102.0/255 blue:93.0/255 alpha:1.0]];
        [self addLabelWithFrame:CGRectMake(39.0, 239.0, 230.0, 2.0) tag:2005 title:@"---------------------------------------------------------" font:[UIFont fontWithName:@"Times New Roman" size:12] color:[UIColor colorWithRed:112.0/255 green:102.0/255 blue:93.0/255 alpha:1.0]];
        [self addLabelWithFrame:CGRectMake(39.0, 264.0, 230.0, 2.0) tag:2005 title:@"---------------------------------------------------------" font:[UIFont fontWithName:@"Times New Roman" size:12] color:[UIColor colorWithRed:112.0/255 green:102.0/255 blue:93.0/255 alpha:1.0]];
        
        [self addLabelWithFrame:CGRectMake(110.0, 176.0, 100.0, 15.0) tag:2001 title:@"adidas | Levi's" font:[UIFont fontWithName:@"Times New Roman" size:12] color:[UIColor colorWithRed:158.0/255 green:145.0/255 blue:130.0/255 alpha:1.0]];
        [self addLabelWithFrame:CGRectMake(75.0, 200.0, 150.0, 15.0) tag:2002 title:@"Red | Green | Blue | White" font:[UIFont fontWithName:@"Times New Roman" size:12] color:[UIColor colorWithRed:158.0/255 green:145.0/255 blue:130.0/255 alpha:1.0]];
        [self addLabelWithFrame:CGRectMake(75.0, 224.0, 100.0, 15.0) tag:2003 title:@"S | M | L | XL | XXL" font:[UIFont fontWithName:@"Times New Roman" size:11] color:[UIColor colorWithRed:158.0/255 green:145.0/255 blue:130.0/255 alpha:1.0]];
        [self addLabelWithFrame:CGRectMake(65.0, 249.0, 80.0, 15.0) tag:2004 title:@"Rs 8000" font:[UIFont fontWithName:@"Times New Roman" size:11] color:[UIColor colorWithRed:158.0/255 green:145.0/255 blue:130.0/255 alpha:1.0]];
        
        [self addLabelWithFrame:CGRectMake(55.0, 270.0, 100.0, 15.0) tag:2004 title:@"save in my stock" font:[UIFont fontWithName:@"Times New Roman" size:11] color:[UIColor colorWithRed:86.0/255 green:58.0/255 blue:22.0/255 alpha:1.0]];
        
        [self addLabelWithFrame:CGRectMake(0.0, 7.0, 80.0, 15.0) tag:2004 title:@"adidas " font:[UIFont fontWithName:@"Times New Roman" size:11] color:[UIColor colorWithRed:112.0/255 green:102.0/255 blue:93.0/255 alpha:1.0]];
        [self addButtonWithFrame:CGRectMake(40.0, 270.0, 17.0, 16.0) tag:1011 title:@"" image:[UIImage imageNamed:@"Savestock"]image:[UIImage imageNamed:@""]color:[UIColor colorWithRed:252.0/255 green:237.0/255 blue:193.0/255 alpha:1.0]];
        [self addButtonWithFrame:CGRectMake(194.0, 270.0, 79.0, 18) tag:1012 title:@"        Purchase" image:[UIImage imageNamed:@"Purchase_button"]image:[UIImage imageNamed:@""]color:[UIColor colorWithRed:252.0/255 green:237.0/255 blue:193.0/255 alpha:1.0]];
        UIImage *sliderBGImage=[UIImage imageNamed:@"slider_bg"];
        UIImage *thumbImage=[UIImage imageNamed:@"slider_tracker"];
        [self addSliderWithFrame:CGRectMake(215.0, 210.0, sliderBGImage.size.width, sliderBGImage.size.height) BGImage:sliderBGImage trackerImg:thumbImage];
    }
}



@end
