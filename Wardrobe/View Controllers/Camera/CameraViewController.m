//
//  CameraViewController.m
//  Wardrobe
//
//  Created by Chetan Rajauria on 14/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "CameraViewController.h"
#import "CoordiViewController.h"
#import "SNSViewController.h"
#import "NewsViewController.h"
#import "SettingsViewController.h"
#import "Registration.h"
#import "AppDelegate.h"
@interface CameraViewController ()
{
    BOOL isCameraShown,isNeedToDisAppear;
}
@end

@implementation CameraViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        //[CameraViewController release];
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    isCameraShown=NO;
    isNeedToDisAppear=NO;
    [super viewDidLoad];
    //[self addButtonWithFrame:CGRectMake(120.0, 200.0, 120.0, 20.0) tag:1001 title:@"Registration" image:[UIImage imageNamed:@"Reg"]];
    [self.topLogoBar setHidden:YES];
    [self.bottomBar setHidden:YES];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    if (!isCameraShown) {
        AppDelegate *app=(AppDelegate*)[UIApplication sharedApplication].delegate;
        [app.tabController hidesTabBar:YES animated:NO];
    
        [self.view setBackgroundColor:[UIColor blackColor]];
        [self performSelector:@selector(showCamera) withObject:nil afterDelay:0.0];

    }
}
-(void)showCamera
{
    UIImagePickerController *picker=[[UIImagePickerController alloc]init];
    picker.delegate=(id)self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        isCameraShown=YES;
       
        [picker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [self presentViewController:picker animated:NO completion:^{
    }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"Camera is not Available." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }

}
-(void)viewWillAppear:(BOOL)animated
{
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    if (isNeedToDisAppear) {
        isCameraShown=NO;
        isNeedToDisAppear=NO;
        [self.bottomBar setHidden:NO];

        AppDelegate *app=(AppDelegate*)[UIApplication sharedApplication].delegate;
        
        [app.tabController hidesTabBar:NO animated:NO];

    }
}
-(void)addButtonWithFrame:(CGRect)frame tag:(int)tag title:(NSString*)title image:(UIImage *)image
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:@selector(buttonAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    button.frame = frame;
    button.tag=tag;
    UIImage * buttonimg = image;
    [button setBackgroundImage:buttonimg forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithRed:191.0/255 green:50.0/255 blue:59.0/255 alpha:1.0] forState:UIControlStateNormal];
    [self.view addSubview:button];
}

-(IBAction)buttonAction:(id)sender
{
    Registration * reg = [[Registration alloc]init];
    reg.hidesBottomBarWhenPushed =YES;
    [self.navigationController pushViewController:reg animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    isNeedToDisAppear=YES;
    [picker dismissViewControllerAnimated:NO completion:^{
        
    }];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    AppDelegate *app=(AppDelegate*)[UIApplication sharedApplication].delegate;
    [picker dismissViewControllerAnimated:NO completion:^{
        
    }];
    isNeedToDisAppear=YES;
    [app.tabController.tabBar selectTabAtIndex:1];
    [app.tabController hidesTabBar:NO animated:NO];
    
}
#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    isCameraShown=NO;
    isNeedToDisAppear=NO;
    [self.bottomBar setHidden:NO];

    AppDelegate *app=(AppDelegate*)[UIApplication sharedApplication].delegate;
    [app.tabController.tabBar selectTabAtIndex:1];

    [app.tabController hidesTabBar:NO animated:NO];
}
@end
