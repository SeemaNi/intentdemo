//
//  Registration.m
//  Wardrobe
//
//  Created by Chetan Rajauria on 21/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "Registration.h"
#import "CoordiViewController.h"

@interface Registration ()
{
}
@end

@implementation Registration

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.tabBarController.tabBar.hidden = YES;
    self.hidesBottomBarWhenPushed = YES;
    [self addColorViewWithFrame:CGRectMake(0.0, 62.0, 320.0, 417) BGColor:[UIColor colorWithRed:241.0/255 green:232.0/255 blue:217.0/255 alpha:0.3]];
    
    [self addSeparatorLineWithFrame:CGRectMake(10.0, 100.0, 300.0, 1.0) image:[UIImage imageNamed:@"dotted-line"]];
    [self addSeparatorLineWithFrame:CGRectMake(10.0, 144.0, 300.0, 1.0) image:[UIImage imageNamed:@"dotted-line"]];
    [self addSeparatorLineWithFrame:CGRectMake(10.0, 179.0, 300.0, 1.0) image:[UIImage imageNamed:@"dotted-line"]];
    [self addSeparatorLineWithFrame:CGRectMake(10.0, 217.0, 300.0, 1.0) image:[UIImage imageNamed:@"dotted-line"]];
    [self addSeparatorLineWithFrame:CGRectMake(10.0, 254.0, 300.0, 1.0) image:[UIImage imageNamed:@"dotted-line"]];
    [self addSeparatorLineWithFrame:CGRectMake(10.0, 295.0, 300.0, 1.0) image:[UIImage imageNamed:@"dotted-line"]];
    [self addSeparatorLineWithFrame:CGRectMake(10.0, 339.0, 300.0, 1.0) image:[UIImage imageNamed:@"dotted-line"]];
    [self addSeparatorLineWithFrame:CGRectMake(10.0, 388.0, 300.0, 1.0) image:[UIImage imageNamed:@"dotted-line"]];
    
    [self addLabelWithFrame:CGRectMake(26.0, 80.0, 100.0, 10.0) tag:3001 title:[NSString stringWithFormat:NSLocalizedString(@"registration date", nil)] color:[UIColor colorWithRed:86.0/255 green:62.0/255 blue:36.0/255 alpha:1.0]font:[UIFont fontWithName:@"Times New Roman" size:10]];
    [self addLabelWithFrame:CGRectMake(28.0, 117.0, 60.0, 10.0) tag:3002 title:[NSString stringWithFormat:NSLocalizedString(@"title", nil)] color:[UIColor colorWithRed:86.0/255 green:62.0/255 blue:36.0/255 alpha:1.0]font:[UIFont fontWithName:@"Times New Roman" size:10]];
    [self addLabelWithFrame:CGRectMake(58.0, 154.0, 40.0, 10.0) tag:3003 title:[NSString stringWithFormat:NSLocalizedString(@"men", nil)] color:[UIColor colorWithRed:86.0/255 green:62.0/255 blue:36.0/255 alpha:1.0]font:[UIFont fontWithName:@"Times New Roman" size:10]];
    [self addLabelWithFrame:CGRectMake(140.0,154.0, 60.0, 10.0) tag:3004 title:[NSString stringWithFormat:NSLocalizedString(@"ladies", nil)] color:[UIColor colorWithRed:86.0/255 green:62.0/255 blue:36.0/255 alpha:1.0]font:[UIFont fontWithName:@"Times New Roman" size:10]];
    [self addLabelWithFrame:CGRectMake(249.0, 154.0, 50.0, 10.0) tag:3005 title:[NSString stringWithFormat:NSLocalizedString(@"kids", nil)] color:[UIColor colorWithRed:86.0/255 green:62.0/255 blue:36.0/255 alpha:1.0]font:[UIFont fontWithName:@"Times New Roman" size:10]];
    [self addLabelWithFrame:CGRectMake(48.0, 194.0, 60.0, 10.0) tag:3006 title:[NSString stringWithFormat:NSLocalizedString(@"parts", nil)] color:[UIColor colorWithRed:86.0/255 green:62.0/255 blue:36.0/255 alpha:1.0]font:[UIFont fontWithName:@"Times New Roman" size:10]];
    [self addLabelWithFrame:CGRectMake(205.0, 194.0, 70.0, 10.0) tag:3007 title:[NSString stringWithFormat:NSLocalizedString(@"coordi", nil)] color:[UIColor colorWithRed:86.0/255 green:62.0/255 blue:36.0/255 alpha:1.0]font:[UIFont fontWithName:@"Times New Roman" size:10]];
    [self addLabelWithFrame:CGRectMake(26.0, 230.0, 80.0, 10.0) tag:3008 title:[NSString stringWithFormat:NSLocalizedString(@"category", nil)] color:[UIColor colorWithRed:86.0/255 green:62.0/255 blue:36.0/255 alpha:1.0]font:[UIFont fontWithName:@"Times New Roman" size:10]];
    [self addLabelWithFrame:CGRectMake(26.0, 269.0, 60.0, 10.0) tag:3009 title:[NSString stringWithFormat:NSLocalizedString(@"taste", nil)] color:[UIColor colorWithRed:86.0/255 green:62.0/255 blue:36.0/255 alpha:1.0]font:[UIFont fontWithName:@"Times New Roman" size:10]];
    [self addLabelWithFrame:CGRectMake(26.0, 312.0, 60.0, 10.0) tag:3009 title:[NSString stringWithFormat:NSLocalizedString(@"comment", nil)]  color:[UIColor colorWithRed:86.0/255 green:62.0/255 blue:36.0/255 alpha:1.0]font:[UIFont fontWithName:@"Times New Roman" size:10]];
    [self addLabelWithFrame:CGRectMake(28.0, 356.0, 60.0, 10.0) tag:3010 title:[NSString stringWithFormat:NSLocalizedString(@"share", nil)]  color:[UIColor colorWithRed:86.0/255 green:62.0/255 blue:36.0/255 alpha:1.0]font:[UIFont fontWithName:@"Times New Roman" size:10]];
    [self addLabelWithFrame:CGRectMake(145.0, 361.0, 60.0, 15.0) tag:3010 title:[NSString stringWithFormat:NSLocalizedString(@"Yes", nil)]  color:[UIColor colorWithRed:105.0/255 green:63.0/255 blue:12.0/255 alpha:1.0]font:[UIFont fontWithName:@"Times New Roman" size:16]];
    [self addLabelWithFrame:CGRectMake(285.0, 361.0, 60.0, 15.0) tag:3010 title:[NSString stringWithFormat:NSLocalizedString(@"No", nil)] color:[UIColor colorWithRed:105.0/255 green:63.0/255 blue:12.0/255 alpha:1.0]font:[UIFont fontWithName:@"Times New Roman" size:16]];
    
    [self addButtonWithFrame:CGRectMake(46.0, 154.0, 13.0, 13.0) tag:1005 title:@"" image:[UIImage imageNamed:@"radio_button"]];
    [self addButtonWithFrame:CGRectMake(126.0, 154.0, 13.0, 13.0) tag:1005 title:@"" image:[UIImage imageNamed:@"radio_button"]];
    [self addButtonWithFrame:CGRectMake(235.0, 154.0, 13.0, 13.0) tag:1005 title:@"" image:[UIImage imageNamed:@"radio_button"]];
    [self addButtonWithFrame:CGRectMake(34.0, 194.0, 13.0, 13.0) tag:1005 title:@"" image:[UIImage imageNamed:@"radio_button"]];
    [self addButtonWithFrame:CGRectMake(191.0, 194.0, 13.0, 13.0) tag:1005 title:@"" image:[UIImage imageNamed:@"radio_button"]];
    
    [self addButtonWithFrame:CGRectMake(253.0, 18.0, 55.0, 25.0) tag:1001 title:@"" image:[UIImage imageNamed:@"back_button"]];
    
//    [self addButtonWithFrame:CGRectMake(52.0, 154.0, 10.0, 10.0) tag:1005 title:@"" image:[UIImage imageNamed:@"radiobutton"]];
//    [self addButtonWithFrame:CGRectMake(52.0, 154.0, 10.0, 10.0) tag:1005 title:@"" image:[UIImage imageNamed:@"radiobutton"]];
//    [self addButtonWithFrame:CGRectMake(52.0, 154.0, 10.0, 10.0) tag:1005 title:@"" image:[UIImage imageNamed:@"radiobutton"]];
//    [self addButtonWithFrame:CGRectMake(52.0, 154.0, 10.0, 10.0) tag:1005 title:@"" image:[UIImage imageNamed:@"radiobutton"]];
    
    [self addTextFieldWithFrame:CGRectMake(87.0, 112.0, 190.0, 22.0) tag:2001 title:@""];
    [self addTextFieldWithFrame:CGRectMake(87.0, 307.0, 190.0, 22.0) tag:2002 title:@""];
    
    UIImage *sliderBGImage=[UIImage imageNamed:@"on-off-sliderbg"];
    UIImage *thumbImage=[UIImage imageNamed:@"on-off-slider-1"];
    [self addSliderWithFrame:CGRectMake(182.0, 356.0, sliderBGImage.size.width, sliderBGImage.size.height) BGImage:sliderBGImage trackerImg:thumbImage];

    
    // Do any additional setup after loading the view.
}


-(void)addLabelWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title color:(UIColor *)color font:(UIFont *)font
{
    UILabel* label = [[UILabel alloc] initWithFrame: frame];
    [label setText: title];
    label.tag =tag;
    label.textColor = color;
    label.font = font;
//    [UIFont fontWithName:@"Times New Roman" size:10];
    //[label setTextColor: [UIColor redColor]];
    [self.view addSubview: label];
}

-(void)addTextFieldWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title
{
    UITextField *tf = [[UITextField alloc] initWithFrame:frame];
    tf.textColor = [UIColor blackColor];
    tf.font = [UIFont fontWithName:@"Times New Roman" size:12];
    tf.backgroundColor=[UIColor colorWithRed:252.0/255 green:237.0/255 blue:222.0/255 alpha:1.0];
    //[tf.layer setCornerRadius:6.50];
    tf.tag=tag;
    tf.layer.borderColor=[[UIColor colorWithRed:144.0/255 green:110.0/255 blue:75.0/255 alpha:1.0]CGColor];
    tf.layer.borderWidth= 1.0f;
    [tf setDelegate:(id)self];
    [self.view addSubview:tf];
}

-(void)addSeparatorLineWithFrame:(CGRect)frame image:(UIImage *)image
{
    UIImageView *lineImage=[[UIImageView alloc]initWithFrame:frame];
    [lineImage setImage:image];
    [self.view addSubview:lineImage];
   // [lineView setBackgroundColor:[UIColor colorWithPatternImage:dotted_line]];
    //[self.view addSubview:lineView];
    [self.view bringSubviewToFront:lineImage];

}


-(void)addColorViewWithFrame:(CGRect)frame BGColor:(UIColor *)color
{
    UIView * boxView=[[UIView alloc]initWithFrame:frame];
    [boxView setBackgroundColor:color];
    [boxView setTag:84948];
  // [boxView.layer setOpacity:120.0];
   // [boxView.layer setOpaque:YES];
    [self.view addSubview:boxView];
    
}
-(UIView*)getListView
{
    UIView *view=[self.view viewWithTag:84948];
    return view;
}
-(void)addButtonWithFrame:(CGRect)frame tag:(int)tag title:(NSString*)title image:(UIImage *)image
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:@selector(buttonAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    button.frame = frame;
    button.tag=tag;
//    button.layer setOpacity:<#(float)#>
    UIImage * buttonimg = image;
    [button setBackgroundImage:buttonimg forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithRed:191.0/255 green:50.0/255 blue:59.0/255 alpha:1.0] forState:UIControlStateNormal];
    [self.view addSubview:button];
}

-(void)addSliderWithFrame:(CGRect)frame BGImage:(UIImage*)image trackerImg:(UIImage*)trackerImage
{
    UISlider *slider=[[UISlider alloc]initWithFrame:frame];
//    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI_2);
//    slider.transform = trans;
    [slider setThumbImage:trackerImage forState:UIControlStateNormal];
    [slider setMaximumTrackImage:image forState:UIControlStateNormal];
    [slider setMinimumTrackImage:image forState:UIControlStateNormal];
    [self.view addSubview:slider];
}
-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(IBAction)buttonAction:(UIButton *)sender
{
    if (sender.tag == 1001)
    {
        CoordiViewController * coordi = [[CoordiViewController alloc]init];
          [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:0] animated:YES];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
