//
//  CommonViewController.h
//  Wardrobe
//
//  Created by Rohit Kumar on 19/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonViewController : UIViewController
@property(nonatomic,strong)UIImageView *topLogoBar;
@property(nonatomic,strong)UIButton *backButton;
@property(nonatomic,strong)UIButton *sideMenuButton;
@property(nonatomic,strong)UIImageView *bottomBar;

@end
